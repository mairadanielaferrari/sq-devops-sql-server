<powershell>
$ServiceName="MSSQLSERVER"
$AgentServiceName="SQLSERVERAGENT"

############################################################################################################################
# Add local Windows Administrator to sql
############################################################################################################################
write-host "Adding local windows administrator to sql..."

$sql=@'
"if not exists(select * from sys.server_principals where name='BUILTIN\administrators')
  CREATE LOGIN [BUILTIN\administrators] FROM WINDOWS;EXEC master..sp_addsrvrolemember @loginame = N'BUILTIN\administrators', @rolename = N'sysadmin'"
'@

stop-service $ServiceName -force
NET START $ServiceName /m
sqlcmd -Q $sql
stop-service $ServiceName -force

NET START $ServiceName
NET START $AgentServiceName
############################################################################################################################

############################################################################################################################
# Create sql users for web and api
############################################################################################################################

$bucketName = "${bucket_name}"
$dbName  = "SquirtPrimary"
$rootPath = "C:\Squirt\"
$scriptPath ="$rootPath\Scripts\"
$settingsFileName = "settings.json"
$webUsername="${sql-web-username}"
$webPassword="${sql-web-password}"
$apiUsername="${sql-api-username}"
$apiPassword="${sql-api-password}"

Write-host "Creating database users..."
$sqlCmd = @"
use [$dbName];

CREATE LOGIN [$webUsername] WITH PASSWORD = '$webPassword', CHECK_POLICY = OFF;
CREATE USER [$webUsername] FOR LOGIN [$webUsername];
EXEC sp_addrolemember N'db_owner', N'$webUsername';

CREATE LOGIN [$apiUsername] WITH PASSWORD = '$apiPassword', CHECK_POLICY = OFF;
CREATE USER [$apiUsername] FOR LOGIN [$apiUsername];
EXEC sp_addrolemember N'db_owner', N'$apiUsername';
"@

sqlcmd -Q $sqlCmd
############################################################################################################################

############################################################################################################################
# Updating system parameters when data becomes available in setting.json populated by other scripts
############################################################################################################################
$settingsUpdated=$false

while($settingsUpdated -eq $false){
  Copy-S3Object -BucketName $bucketName -key $settingsFileName -file $scriptPath\$settingsFileName
  $settingsJson = Get-Content("$scriptPath\$settingsFileName") | Out-String | ConvertFrom-Json
  $apiDns = $settingsJson."api-alb-dns"
  $webDns = $settingsJson."web-alb-dns"

  if (($apiDns) -and $($webDns)) {
    $apiUrl = "https://$apiDns"
    $sqlCmd = "update $dbName.dbo.system_parameters set parameter_value = '$apiUrl' where id = 441"
    sqlcmd -Q $sqlCmd

    $webUrl = "https://$webDns"
    $sqlCmd =  "update $dbName.dbo.system_parameters set parameter_value = '$webUrl' where id in (12, 109, 702, 711, 712)"
    sqlcmd -Q $sqlCmd

    $settingsUpdated=$true
  } else {

    $msg =  @"
$(Get-Date -Format g) waiting for data in s3 bucket. api-alb-dns: $apiDns, web-alb-dns: $webDns
"@
    write-host $msg
    Add-Content $scriptPath\user-data.log $msg
    Start-Sleep -s 10
  }
}
############################################################################################################################
</powershell>               

