locals {
  private-subnets        = "${var.subnets["private"]}"
}

resource "random_string" "sql-web-password" {
  length = 16
  special = false
}

resource "random_string" "sql-api-password" {
  length = 16
  special = false
}

data "template_file" "user-data" {
  template = "${file("user-data.ps1")}"
  vars {
    bucket_name         = "${var.s3-bucket-name}"
    sql-web-username    = "${var.sql-web-username}"
    sql-web-password    = "${random_string.sql-web-password.result}"
    sql-api-username    = "${var.sql-api-username}"
    sql-api-password    = "${random_string.sql-api-password.result}"
  }
}

resource "aws_instance" "sql-server" {
  ami                           = "${var.sql-server-ami-id}"
  instance_type                 = "${var.sql-server-instance-type}"
  key_name                      = "${var.sql-server-key-pair}"
  subnet_id                     = "${element(local.private-subnets, 0)}"
  associate_public_ip_address   = false
  security_groups               = ["${var.sql-server-sg-id}"]
  iam_instance_profile          = "${var.ec2-instance-profile-name}"
  user_data                     = "${data.template_file.user-data.rendered}"
  
  tags {
    Name            = "${var.deployment-prefix}-sql-server"
    Organization    = "${var.organization}"
    Project         = "${var.project}"
    Environment     = "${var.environment}"
  }
}

