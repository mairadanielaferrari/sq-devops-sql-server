# Creds & Region
variable "aws-access-key" {}
variable "aws-secret-key" {}
variable "aws-region" {}

# Vpc Id
variable "vpc-id" {}

# Subnets
variable "subnets" {
  type="map"
}

# Security Group
variable "sql-server-sg-id" {}

# Naming and tagging
variable "deployment-prefix" {}
variable "organization" {}
variable "project" {}
variable "environment" {}

# Sql Server
variable "sql-server-instance-type" {}
variable "sql-server-ami-id" {}
variable "sql-server-key-pair" {}
variable "ec2-instance-profile-name" {}
variable "sql-api-username" {}
variable "sql-web-username" {}

# S3 Bucket
variable "s3-bucket-name" {}
variable "s3-settings-path" { 
 default = "settings.json"
}