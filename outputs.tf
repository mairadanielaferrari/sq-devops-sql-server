output "sql-server-dns" {
  value = "${aws_instance.sql-server.private_dns}"
}

output "sql-web-username" {
  value = "${var.sql-web-username}"
}

output "sql-web-password" {
  value = "${random_string.sql-web-password.result}"
}

output "sql-api-username" {
  value = "${var.sql-api-username}"
}
output "sql-api-password" {
  value = "${random_string.sql-api-password.result}"
}